﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gameManager : MonoBehaviour {
	public GameObject MarkerAll;
	public GameObject Cube;
	public GameObject Cylinder;
	public GameObject ui_CLEAR;
	public GameObject HitFx;

	private int PutOBJs = 20;
	public GameObject X_value;
	public GameObject Y_value;

	public Camera     myCamera;
	public GameObject MSG;

	private bool GameClearJudgement;

	// Use this for initialization
	void Start () {
		X_value.GetComponent<Text>().text = "0";
		Y_value.GetComponent<Text>().text = "0";

		Vector3 Cyl_ScrPos = myCamera.WorldToScreenPoint(Cylinder.transform.position);
		MSG.GetComponent<Text>().text = (Cyl_ScrPos.x.ToString() + "," + Cyl_ScrPos.y.ToString() + "," + Cyl_ScrPos.z.ToString());

		GameObject[] Marker = new GameObject[20];
		for(int i=0; i<PutOBJs; i++){
			float x = Random.value * 10 - 5; // -5~5のランダム
			float y = Random.value * 5; // 0~5のランダム
			float z = Random.value * 5; // 0~5のランダム

			float rot_x = Random.value * 360;
			float rot_y = Random.value * 360;
			float rot_z = Random.value * 360;

			Marker[i] = (GameObject)Instantiate(Cylinder, new Vector3(x,y,z), Quaternion.Euler(rot_x,rot_y,rot_z) );
			Marker[i].transform.parent = MarkerAll.transform;
		}
		GameClearJudgement = true;

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			Vector2 ScreenPosition = Input.mousePosition;

			X_value.GetComponent<Text>().text = ScreenPosition.x.ToString();
			Y_value.GetComponent<Text>().text = ScreenPosition.y.ToString();

		
			Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit = new RaycastHit();
			if(Physics.Raycast(ray , out hit)){
				GameObject Obj = hit.collider.gameObject;
				Instantiate(HitFx,Obj.transform.position,Quaternion.Euler(-90,0,0));
				Destroy(Obj);
			}

		}

		if(GameClearJudgement){
			int remainMarkers = MarkerAll.transform.childCount;
			if(remainMarkers == 0){
				ui_CLEAR.SetActive(true);
				Debug.Log("Game Clear!!");
				GameClearJudgement = false;
			}
		}
	}
}
